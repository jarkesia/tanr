  /* A very very complex AVR synth (/w ATtiny85).
 Copyright (C) 2013-2016  Jari Suominen
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 /*
 This software is currently only supporting Attiny85 chip.
 This tutorial will tell you how to flash the code to the chip.
 http://highlowtech.org/?p=1695 
 */

/*
           T T T T T T T T T T T T T T T T T T T T T T T T T T T T T T T  
        A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A  A                                
     N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N   N                               
 R    R    R    R    R    R    R    R    R    R    R    R    R    R    R    R    R                           
*/

/*
                                   ||`-.___
                                   ||    _.>
                                   ||_.-'
               ==========================================
                `.:::::::.       `:::::::.       `:::::::.
                  \:::::::.        :::::::.        :::::::\
                   L:::::::         :::::::         :::::::L
                   J::::::::        ::::::::        :::::::J
                    F:::::::        ::::::::        ::::::::L
                    |:::::::        ::::::::        ::::::::|
                    |:::::::        ::::::::        ::::::::|     .---.
                    |:::::::        ::::::::        ::::::::|    /(@  o`.
                    |:::::::        ::::::::        ::::::::|   |    /^^^
     __             |:::::::        ::::::::        ::::::::|    \ . \vvv
   .'_ \            |:::::::        ::::::::        ::::::::|     \ `--'
   (( ) |           |:::::::        ::::::::        ::::::::|      \ `.
    `/ /            |:::::::        ::::::::        ::::::::|       L  \
    / /             |:::::::        ::::::::        ::::::::|       |   \
   J J              |:::::::        ::::::::        ::::::::|       |    L
   | |              |:::::::        ::::::::        ::::::::|       |    |
   | |              |:::::::        ::::::::        ::::::::|       F    |
   | J\             F:::::::        ::::::::        ::::::::F      /     |
   |  L\           J::::::::       .::::::::       .:::::::J      /      F
   J  J `.     .   F:::::::        ::::::::        ::::::::F    .'      J
    L  \  `.  //  /:::::::'      .::::::::'      .::::::::/   .'        F
    J   `.  `//_..---.   .---.   .---.   .---.   .---.   <---<         J
     L    `-//_=/  _  \=/  _  \=/  _  \=/  _  \=/  _  \=/  _  \       /
     J     /|  |  (_)  |  (_)  |  (_)  |  (_)  |  (_)  |  (_)  |     /
      \   / |   \     //\     //\     //\     //\     //\     /    .'
       \ / /     `---//  `---//  `---//  `---//  `---//  `---'   .'
________/_/_________//______//______//______//______//_________.'_________
##########################################################################
*/

#include <EEPROM.h>
#include <avr/sleep.h>

#define ENABLE_LINE_IN_____
#define ENABLE_ELEKTRIK____

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#define SPEAKER 0
#define LINE_IN 1
#define TOUCH_1 3
#define TOUCH_2 4

#define PROGRAM_1B 0
#define PROGRAM_SH 2
#define PROGRAM_GTR 1
#define PROGRAM_ELEKTRIK 3

#define ARP_SPEED 200

volatile uint8_t portBHistory = 0xFF;


int a = 0;    // This accumulator is running slower
int b = 0;    // This is slow too!

long ticker = 0;
int arp_counter = 0;
uint8_t changedBits;

boolean mute;
byte program;

byte param1 = 60;
byte param2 = 20;

byte ps = 5;

/*


  H   3  7   15  31  63  128  
  B          16  33  67  135  
  A      8   17  35  71  
  Ab                 76
  G                  80
  Gb             42  85
  F                  90
  E   5  11  23  47  95
  Eb             50  101
  D          26  53  107
  Dd             56  114
  C              60  120  
  
 */
int song1[] = {95,180,0,20,95,80,47,100,67,100,71,300};
int songIterator = 0;

void setup() {

  pinMode(SPEAKER,OUTPUT);
#ifdef ENABLE_LINE_IN
  pinMode(LINE_IN,INPUT);
#endif
  pinMode(TOUCH_1,INPUT);
  pinMode(TOUCH_2,INPUT);

  /* These lines switch off all functions
   recarding ADCs which will take power consumption
   down close to 1uA when put to sleep */
  //ADCSRA &= ~(1<<ADEN);
  //ACSR |= _BV(ACD);
  //MCUCR |= _BV(7) | _BV(2);
  
  delay(100);
  
  int val = analogRead(A1);

  
  if (val < 500) program = PROGRAM_SH;
#ifdef ENABLE_ELEKTRIK
  else if (val < 500) program = PROGRAM_ELEKTRIK;
#endif
#ifdef ENABLE_LINE_IN
  else if (val < 800) program = PROGRAM_GTR;
#endif
  else program = PROGRAM_1B;
  
  //program = PROGRAM_ELEKTRIK;
  
  pinMode(0,OUTPUT);
  portBHistory = PINB;
  
  sbi(GIMSK,PCIE);   // Turn on Pin Change interrupt

  switch (program) {
    case PROGRAM_1B:
      initCTC();      
      sbi(PCMSK,PCINT4); 
      sbi(PCMSK,PCINT3);
      break;
    case PROGRAM_ELEKTRIK:
      TCCR0B = B00000101;
      TCCR0A = B01000010; // CTC mode     
      sbi(PCMSK,PCINT4); 
      sbi(PCMSK,PCINT3);
      OCR0A = song1[0];
      break;
    case PROGRAM_GTR:
      TCCR0B = B00000011;
      TCCR0A = B00000010;
      sbi(PCMSK,PCINT4);
      sbi(PCMSK,PCINT3);
      sbi(PCMSK,PCINT1);
      break;
    case PROGRAM_SH:
      TCCR0B = B00000101; // prescaler 64
      TCCR0A = B01000010; // CTC mode
      OCR0A = B11111111;
      sbi(PCMSK,PCINT4);
      sbi(PCMSK,PCINT3);
      break;
  }
  
  //OCR0A = 40 * program + 10;
}

void initCTC() {
  TCCR0B = B00000011; // prescaler 64
  TCCR0A = B01000010; // CTC mode
  OCR0A=B11111111; 
}



/*
  Main loop is just taking care of scanning the potentiometer and changing
  the program used in audio synthesis accordingly.
 */

int p = 0;          /* subprogram */
int lastRead = -1;
int loopTicker;
byte pitch;
byte divider = 1;

long lastNoteTicks = 0;
long ticks = 0;

void loop() {
  int val = analogRead(A1);

  p = map(val,0,1024,0,ps); // We have programs from 0 to ps!
  byte parameterCandidate = (byte)map((val-(int)map(p,0,ps,0,1024)),0,1025/ps,1,255); // [1,255]
  
  switch (program) {
    case PROGRAM_1B:
      {      
        if (abs(val-lastRead) > 4) {
          lastRead = val;
          OCR0A = OCR0A + 1; 
        }
      }
      break;
    case PROGRAM_ELEKTRIK:
      {      
        ticks++;
      //if (abs(val-lastRead) > 4) {
      //  lastRead = val;
      //OCR0A = (byte)millis();
        if (ticks - lastNoteTicks > song1[songIterator+1]*3) {
          songIterator += 2;
          if (songIterator >= (sizeof(song1)/sizeof(*song1))) songIterator = 0;
          TCCR0B = B00000101;
          OCR0A = song1[songIterator];
          lastNoteTicks = ticks; 
        }
        
          
      //}
      }
      break;
    case PROGRAM_GTR:
      if (p != 0) TCCR0A = B00000010;
      if (p == 2 || p == 1) param1 = map(parameterCandidate,1,255,10,220);
      else param1 = map(val,614,1023,7,2); // Divider Fuzz
      break;
    case PROGRAM_SH: {
      if (abs(val-lastRead) > 4) {
        lastRead = val; 
        pitch = val & B11111111;
        //divider = 1;
        OCR0A = pitch / divider;
        TCCR0B = (val >> 8) + 2;
      }
    }
    break;
  }
}


/*
  Pin change interupt. Here the magic is happening.
 */


int t = 1;   // 8-bit accumulator for sound synthesis
byte tmp = 2;

ISR(PCINT0_vect) {
  cli();
  ticker++;
  changedBits = PINB ^ portBHistory;
  portBHistory = PINB;
  
  /*
    Audio synthesis part. Audio synthesis on TANR is based on concept of
    one line symphonies by Finnish demoscene legend Viznut.
    http://countercomplex.blogspot.is/2011/10/algorithmic-symphonies-from-one-line-of.html
    The original symphonies are audio loops generated by one liners, typically
    consisting of cryptic low level bitwise arithmetic. These lines are not invented
    they are founded by random tinkering. One of the first one liners found were:
    
      t*((t>>12|t>>8)&63&t>>4);           // discovered by: viznut
      (t*(t>>5|t>>8))>>(t>>16);           // discovered by: tejeez
      t*((t>>9|t>>13)&25&t>>6);           // discovered by: visy
      t*(t>>11&t>>8&123&t>>3);            // discovered by: tejeez
      (t>>6|t|t>>(t>>16))*10+((t>>11)&7); // discovered by: viznut
    
    These one liners are not interactive in any sense. Instead they will only
    generate one audio loop that will always remain the same.
    
    TANR audio engine differs from the original concept in few different ways.
    Firstly, we are not driving signals to the PCM output. We could do that,
    and will probably add modes that would do it, but on AVR, CTC-mode sounds
    hifier and more bleepy. This means that we will not use the value calculated
    from the one liner to change amplitude of PCM signal. Instead we are just
    adjusting the frequency of the square wave oscillator.
    
    Other difference is that we are updating the value on the equation with
    alternating times that are defined by RC network consisting of a capacitor,
    resistor and the skin impedance of the player of the instrument. TANR is
    a touch contact based instrument and this is were the touch contacts step in.
    
    Last bit is cross modulation between touch contacts that will change coefficients
    of one liner that is playing, one liner controlled by the other touch contact
    or both. This will result one liners and the state of the instrument constantly
    change and drift a bit.
    
   */
  
  switch (program) {
    case PROGRAM_1B:
    case PROGRAM_ELEKTRIK: {
      t++;
      if (PROGRAM_ELEKTRIK) TCCR0B = B00000011;
      if (changedBits & (1 << 4)) { // PIN 3 CHANGED!
        switch (p) {
        case 0:
          OCR0A = t*((t>>(a%(b%10))|t>>8)&63&t>>4); // discovered by: viznut
          break;
        case 1:
          OCR0A = (t*(t>>5|t>>8))>>(t>>((b%5)+10));
          break;
        case 2:
          OCR0A = (t*9&t>>(b%4)|t*(a%5)&t>>7|t*3&t/1024)-1;
          break;
        case 3:
          OCR0A = t*5&(t>>(b%7))|t*3&(t*(a%6)>>10);
          break;
        case 4:
          OCR0A = (t>>6|t|t>>(t>>16))*10+((t>>11)&7);
          break;
        }
        if (ticker > ARP_SPEED) {
          ticker = 0;
          a++;
        }
      } 
      else { //if (changedBits & (1 << 3)) { // PIN 2 | PCINT3
        switch (p) {
        case 0:
          OCR0A = (t>>6|t|t>>(t>>16))*10+((t>>11)&7);
          break;
        case 1:
          OCR0A = t*((t>>(a&12)|t>>8)&63&t>>(b%6)); 
          break;
        case 2:
          OCR0A = t*(t^t+(t>>(a%15)|1)^(t-1280^t)>>(b%10));
          break;
        case 3:
          OCR0A = t*((t>>(a%10)|t>>13)&25&t>>(b%5));
          break;
        case 4:
          OCR0A = (t*(t>>(b%7)|t>>8))>>(t>>(a&10+5));
          break;
        }
        if (ticker > ARP_SPEED) {
          ticker = 0;
          b++;
        }
      } 
    }
    break;
    case PROGRAM_GTR:
      {      
        if (changedBits & (1 << 1)) { // PIN 7 | PCINT2
          t++;
          switch (p) {
            case 0: // "RINGMOD"
              if (TCCR0A == B01000010) {
                TCCR0A = B00000010;
                PORTB = PORTB | B00000001;
              }
              else TCCR0A = B01000010;
              break;
            case 1:  // DIVIDER
              /* Here tmp [2,n] */
              if (tmp/2 >= t) {                  
                PORTB = PORTB | B00000001;         
              } else {                          
                PORTB = PORTB & B11111110;  
              }
              if (tmp <= t) t = 0;
              break;
            case 2: // ARPEGGIO
              //TCCR0A = B00000010;
              if (a%2==0 || t%(a%param1+1)==0) PINB = B00000001;
              break;
            case 3:  // DIVIDOR
            case 4:  // FUZZ
              if (param1/2 >= t) {
                PORTB = PORTB | B00000001;             
              } else {
                PORTB = PORTB & B11111110;  
              }
              if (param1 <= t) t = 0;
              break;
          }
        }
        if (ticker > param1) {
          ticker = 0;
          a++;
          //param2++;
          //tmp++;// = param2 >> 5 + 1;//random(1,6);
          if (p==1) {
            switch ((param2 >> 6)) {
              case 0:
                tmp = (a>>6|a|a>>(a>>16))*10+((a>>11)&7);
                tmp = map(tmp,0,255,2,6);
                break;
              case 1:
                tmp = random(2,5);
                break;
              case 2:
                tmp = random(2,8);
                break;
              case 3:
              default:
                tmp = random(2,4);
                break;
            }
          }
          //tmp++;
        }
        if (changedBits & (1 << 4)) { // PIN 3 CHANGED!
          OCR0A = OCR0A+1; 
          TCCR0A = B01000010;
          //if (p==0) {
          //  param1++;
          //  if (param1 < 1) param1 = 1; 
          //}
          if (p == 1) param2++; 
        } else if (changedBits & (1 << 3)) {
          OCR0A = OCR0A-1;
          TCCR0A = B01000010; 
          //if (p==0) {
          //  param1--;
          //  if (param1 < 1) param1 = 1; 
          //}
          if (p == 1) param2++;  
        } else {
          return;  
        } 
      }
      break;
    case PROGRAM_SH:
      if (changedBits & (1 << 4)) { // PIN 3 CHANGED!
        if (ticker > 10) {
          ticker = 0;
          a++;
          divider = random(1,6);
          OCR0A = pitch / divider;
          lastRead = -1;
        }
      } else {
        if (ticker > 20) {
          ticker = 0;
          b++;
          OCR0A = random(255); 
        }  
      }
      break;   
  }

  /* These lines take care of charging and
   recharging the pencil cap. Effectively this will
   emulate 555 based drawdio as accurately as possible.
   */

  DDRB =  DDRB  | B00011000; // cap pin output
  PORTB = PORTB & B11100111; // cap pin low

  byte b = 0;
  while (b < 100) {
    b++; 
    __asm__("nop\n\t"); 
    __asm__("nop\n\t"); 
    __asm__("nop\n\t"); 
    __asm__("nop\n\t");
  }

  DDRB = DDRB & B11100111; // cap pin input 
  PORTB = PORTB | B00000000;

  sei();
  return;
}



/* 

                                        ..   ...
                                     AIMMYIAMMYYII..
                                    AIHMMMHMIHMHYIIIIA
                                  AIHPP/?/ /$$/PVYMJHIIA.
                                .AP//$ ? //$$/ ////VMMHIA.
                               A///$/$??///$ //////?//VMHIA
                              A///$/$/?//// / ////?///?/VHHA .
                             A////$/$??/// ///////?//?//?VMMA .
                             ////$/$ ??/// //////?/////?//VMHIYM..
                            A////$/AMMMMMMHHHA//?///?//?//.IYHMMMM.
                           .H////AMMMHHHMHMMMHHHA//?//?//?$VHHHYYI.
                           AH//AMMHMHHMHMMMHHHHHHIY//?//?$//MHHHMHI.
                            //AHMHMIIMHHIMMMHMYHMIIMI//?$///MMMHHIII
                            /AHHIHMIHIHHHMMMMMHHHMHMMA/$///MMMMHMHII.
                           .MMYYHIIHIIIIHMMMMMMMHHMMMMMM//HMMMHMHHII
                      .AHHHHIMMMM:I::::IIMMMMMYYMMMMMMMMHM/MMHHHMMH
                      AIJMMH/I:MI""MM:..::;;AYMMMM:::MMMM/MMMYHIHHI..
                    AMHIHIIMMHII I"MMM: .::I" MVA:::.MMMMMMYMMHIIIII/
                    AIIHHHHMMMMMII "V.". ::;II;:::...MMMMMMMMMHHIIIII
                    VIHHIHHMMMMMI' '..'  ::;;;::...:HMMMMMMMMMHHHIII.
                       HHHHMMMMM'  ...   :::;;::::::VMMMMMHHMMHHIIIIIII.
                       HIHMMMMMMA   ..: ':::II::::::::MMMMMHHHHHIIHHHHH.
                       HHIHMMMMMM:  .::'":IYI:II::::::MMMMMMMHMMHHHHIIII
                       HHHHHMMMMMA  :I.  .:::::JI::::IMIMHMMMHIHMMMIIIII
                        VHHIMMMMMMA ::YH""""'MP:::..:MMYMMMMMMHHMMHIIII
                        HHHHIYMMMMMA ..VIPHHI".:::::AMMMMMMMMMHIMM:::..
                    .:HMMMHYYHMMMVM$A.   "":::::::AMMMYYMMMMMMMMM::..:.
                     HV AHMMHYMMA/VM/$:    ..::::IIMMMMMMMMMMMMVII:::. :.
                     HHHHIHMMYYMM// $$::..::::IIIIIIMMMMMMMMMMMIIII::::.:
                         HHHM//HM$$ / M:::...::::IIIIYYYYYYHHIIIIII::::::
                      .:::YM//VSMX$$$ /II::...::::IIIII:.IIIII::::::::::
                   .:::.:::/Y/AX/////$MMIII:::IIIIIIIIII.I:::::::::::II
                 .:...::::/VYX//////HH/HII:::.::::::::::..::::::::::III:
                .....::::///////////II//IIIIII::.::::::::.::::::I:::III:
               .....:::I:///////////X///IIIIII:..::::::::..::::IH::::II:
               ..  ::::II//////////X////:::III:..::::::::..::::IM::::II:
              .. .:::IIIIII://////X////::::II:.:::::::::..::::IM::::II:
               .. ...:::IIHIII:::"/////I::::::::::::::::::.::::IM:::::I:
               ....:::::IIIII::::..::://::::::::::::::::::..:::II:::::::
               .....::::IIII:::::..:::::::::..:::::.:::::::..::II:::::::
               ....::::IIIIM"::::..::::::::....:::....:::::..::II:..::::
               :...:::::IIIM::::".:::::::::::........::::::..::HI:..::::
               ::..:::::::IM:::"..::::::::::::.......::::::. .:HMI:..:::
               ::.::::::::IMI::  ..:::::::::::::...:::::::.. ..VMI:..:::
               ::::::::::::II:    .:::::::::II::...::::::..  ...MI:..:::
               :::::::::::IH..    '::::::::IIII::...::::'   . ..MI:...::
               :...::::::::M:'     '::::::IIIII::..:::::'  . . .VI::..::
             .I...::::::::I:      . ::::IIIIIII:::::I:'.  . ..:V I:..:::
          .::::I..::::::::I:        .::IIIIHHHHHII:::    AMI .V  I:..:::
        .:::..:I:..:::::::II AI.   . .::IHHHI:II::'      "WV.V'  I::::::
     .::.......I:::::::::IHM.VMA...:AHIIHHHII::. ..  ..::..:V    ::::..:
    .:::........I::::::::IIIMMMMMMHHHHH....::III:::::::IIIIV     :::.:::
   .::::::::::..I:::::::::IIIMMMMMHHHHHHHHHIIIIHHHMMMHMMHHV      ::::.::
  . :::::::::::::I:::::::::IIAMMMMHHHHHHHHIIIIIIIHHHHHHHHV       ..:...:
 .: ...::::::::::I:::::::::II::VMMMMHHHHHHHIIIIIHHHHHHHHH"        ::...:
 ::. .::;;;;;::::I::.:::::III::IVMMMMMMMHHHHIIIIIIIHHHHV           ..:..
.:::. ':::;;;::::I::..:::::II:::::::VMMMMHHHHHHIIIHMMM"             .. .
:::::.    '::;;:::I...::::IIIIIIII::::VHMMMMHHHHIHMM"               '..
::::::.     ':;;;::I:..::::IIIIM:::::::IIMMMMMMMMMM"                 ...
::::..:::.     ::::I. :::::III':::::IIHHMMMMMMMMM".                  ':.
:::...::::I::   "::I: .::::III.:::::IHHMMMMMMMMMV:.                   ::
:::....:::III:   ":I:. ::::III::::IIIHHHMMMMMMMV:::                   ::
::::...:::;IIIH    :I: ':::IIII:IIHHHMMMMMMHHHII:::                   ':
::::...::::;;IIM.:. I::..::::IIHHHHMMMMMMMHHHII::::                    '
::::...:::::;;IMMWHMI::..::::IIIHHHMMMMMHHHHII:::::                    .
:::::...::::;IIHMMMMM::...::::IIHHHHHHHHHHHHII::::'
:::::..;::::;;IIHMMMMM::...:::::YHHHHHMMMHHIII:::.
:::::...::::;;IIHMMMMMI:...::::I :"""":::IIII::::
::::::..::::;;;IIHMMMMM::..:::::I::::...   . .::'
':::::..::::;;;IIIHMMMVI:..:.:::I::::::;;;;::::
 '::::::::::;;;IIIIMMI::I::..::::I:::::::::::::
  ::::::::::;;;;;IIMH;:::I::..:::I::::::::::::
  ':::::::::;;;;;;IM;::::I:.:..::1I..::::::::'
   :::::::::;;;;;;IM::::::I::...::I::::::::;'
   ':::::::::;;;;IMI:::::::I.:..::.I.:::::;;
    ::::::::;;;;;IM;::.:::::II:.::..::::::;'
    :::::::::;;;IIM::..:::::II:..::.I:::;;
    ':::::::::;;IIM::...:::::II:..:.:;;;;'
     :::::::::;;IH::...::::::III:..:.I;;V
     '::::::::;;IH::...::::::;III.:::.IV
      :::::::::;IV::...::::::::III.:::I.
      ':::::::;;H::...:::::::;;JI:..'::A
       ;::::::;;I::....:::::::;;II...::.I                              /
       :::::::;H::....::::::::;;I:..:':::.                             :
        :::::;;I::...:::::::::;II:..'::::I    .:::....                .:
        ::::::I::....:::::::::;I:....::I:I.:I:::I.::::::::::.  ..   .:II
        ':::::I::..::::::::;;;I:'.::IIHI:I.:IIIIIIIIIIIIIII""/MMM-.:JIHM
        '::::I::...:::::::::;;I":::IIVVI:I.HHI::::::II/MMMMMMMMMM..PPPP:
         ::::I::...::::::::;;;IY::IVMMA:::IH::IA:::AHHMMMVXXXXXVXXII::::
         ::::I:'...:::::::::;;I::VMHHIII:"IH:?HHA:H//. XXXXXXX/"":I:::II
         :::I::...:::::::::;;".:/AI::HHHA.:H?HHHHH/$XX"$$XX.X//.AI::::::
         :::I::...:::::::::;I:/AII:H"."."."?HHHHH/////$'////"///A:::::::
         .::I::..:::::::::;I./A":.:.".".".?HHHHHH//' " . :// //A":::::::
         .::I::..:::::;;;;;./:::.".".".".??HHHH///'.. MI.//M/.AIIII:::::
         :::I::..:::::;;;;."::::."."."."?AHHHHH/////.  //AVHHII:::::::::
         :::II:.:::::;;;;;::-:"."."."./??HHHH/"'$/// '//MVH:::::::::::::
         ::::I::::::;;III":::,:""-.///I?$ .  .$$M/////MM//I,::::::::::::
         :;:;I,,,::;;IIII::::::::/.":::$$$$ $ $////AMM$/III.::::::::::::
         :;;:;I:;IIIII:::::::/$::::$$/////$$XVMV"$$II..:::::::::::::::::
         :::;;;IIIIIIIIII,,,,,,/$.::://///AMM",,::$I::::::::::::::::::::
         ':::;;I;;;IIIIII,,::,,/M,,::,:MMMMV,,,,,,:$$:::::::::::::::::::
          ::::;;I;;IIIIII,,,,,/$I,:,::::::,,,,,,,,/$::::::::::::::::::::
          .:::;;;II;;IIII:::II/II:::: ::::: ,,,,,,/$::::::::::::::::::::
           :::::;;I:::II:.:::::I::IIII"II:"I,,,:,,/$::::::::::::::::::::
           :::::;;II;;;;I..",,,M'":":$$$$$::YYII../$JJJ:::::::::::::::::
           '::::;;II:::;I,,,,,IM,,,: $$$$$$I:::::::$::::YYHHHHHJJJ::::::
            ::::;;III::;I:::::IM,,: $$$$$$/::::::::/:::::::::::::::YYMMM
            ':::;;IIII:I:::::MMMM://$ $$?$$$:::::::/::::::::::::::::::::
             ::;;;IIHH:I::::::XX"/////???$$$III::::/::::::::::::::::::::
             :::;;;IHH:I::::::XX////$.....$$$IIII::/::::::::::::::::::::
             ':::;;IHHHI::::::X////$$....XX$$IIIII/::::::;:;:;::::::::::
              ::;;;IHHHH::::::..///$$...XXXX$HIII:::;:;:;:;;:;:;:;:;:::;
              ':;;IIHHHI::::://   $$$..XX.XX$IIIXIII:;:;:;:;:;:;:;:;:;:;
               ::;;IHHHH:::://///$$$....XXXX$IIIII;:;:;:;:;:;:;:;:;:;;:;
               ':;;IIHHH::://////$$$.......?$$HIIII;:;:;:;:;:;:;:;:;:;:;
                ::;;IHHH::$/////$$$$........$$HIIII:;:;:;:;:;:;:;:;:;:;:
                '::;IHHI:::////$$$$$......X.$$HIII:;:;:;:;:;:;:;:;:;:;:;
                 ::;;IHH::::::/M$$$MMM...HHH$$HHI:;;;:;:;:;:;:;:;:;:;:;:
                 ':;;IHI::::;:;$$$$MM.X.XXXX$HHHHI:;:;:;:;:;:;:;:;:;:;:;
                  :::;HX::::I:I:::X.X....XXX$HHII:;:;:;:;:;:;:;:;:;:;:;:
                  ::::H:::::I:I::::::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;
                  '::IH:::::I:I:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:
                   ::.H::::I:I:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;
                   :;IH::::II:I:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:
                   ;;IH::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;
                   I;IH:::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:
                  .;:IH::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;
                  ;::IH:::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:
                .;;::IH::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;
               .;;::;IH:::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:
             .:;;::;IIH::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;
           .;;;::::;IIH:::::;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:;:
*/

